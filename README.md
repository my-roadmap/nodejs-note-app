# Note App

A minimal note app in NodeJS. Notes are saved as a json file locally under the name `notes.json`.

## Requirements
- node >= 10.0

## Getting started
- Install dependencies with:

 ```
    npm i
```
- Call the app from this directory with help argument for further help:

```
    node app.js --help
```

## Overview
```
Commands:
  app.js add     Add a new note
  app.js remove  Remove a note
  app.js list    List all notes
  app.js read    Read a note

Options:
  --help     Show help                                                 [boolean]
  --version  Show version number                                       [boolean]
```

